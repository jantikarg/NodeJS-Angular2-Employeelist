//importing modules
var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var cors = require('cors');
var path = require('path');// core module so no need to install

var app = express();
app.use(bodyparser.json());//use it before router

var route = require('./routes/route');

//connect to mongoDB
mongoose.connect('mongodb://localhost:27017/employeelist');

//on connection
mongoose.connection.on('connected',()=>{
    console.log('Connected to mongo db @ 27017');
});
//on error
mongoose.connection.on('error',(err)=>{
    if(err){
        console.log('Failed to connect mongo db @ 27017:' +err);    
    }    
});

//port no
const port = 3000;

app.use('/api', route);

//adding middleware
app.use(cors());

//static files
app.use(express.static(path.join(__dirname, 'public')));

//testing Server
app.get('/',(req,resp)=>{
    resp.send('Welcome');
});

app.listen(port, ()=>{
    console.log('Server is running on port:'+port);
});