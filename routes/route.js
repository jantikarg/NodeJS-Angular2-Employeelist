const express = require('express');
const router = express.Router();

const Employee = require('../models/employees');

//reteriving all data
router.get('/employees', (req, resp, next)=>{
    Employee.find(function(err, employees){
        console.log('In get method of server');
        resp.json(employees);
    });
});

//reteriving data by id
router.get('/employee/:id', (req, resp, next)=>{
    Employee.find({_id: req.params.id}, function(err, employees){
        console.log('In getbyid method of server');
        resp.json(employees);
    });
});

//add data
router.post('/employee', (req, resp, next)=>{
    console.log('In Post Method');
    console.log('Request:'+req.body.name);
    let newEmployee = new Employee({
        name: req.body.name,
        email: req.body.email,
        dob: req.body.dob,
        department: req.body.department,
        gender: req.body.gender,
        age: req.body.age
    });

    newEmployee.save((err, employee)=>{
        console.log('In save method of server');
        if(err){
            resp.json({msg: 'Failed to add Employee'});
        }
        else{
            resp.json({msg: 'Employee added successfully'});
        }
    });
});

//Update data
router.put('/employee/:id', (req, resp, next)=>{
    console.log('In Put Method');
    console.log('Request:'+req.body.name);
    let newEmployee = new Employee({
        name: req.body.name,
        email: req.body.email,
        dob: req.body.dob,
        department: req.body.department,
        gender: req.body.gender,
        age: req.body.age
    });

    newEmployee.save({_id: req.params.id}, function(err, employee){
        console.log('Error:'+err);
        if(err){
            resp.json({msg: 'Failed to update Employee'});
        }
        else{
            resp.json({msg: 'Employee updated successfully'});
        }
    });
});

//delete data
router.delete('/employee/:id', (req, resp, next)=>{
    Employee.remove({_id: req.params.id}, function(err, result){
        console.log('In delete method of server');
        if(err){
            console.log('Error:'+err);
            resp.json(err);
        }
        else{
            console.log('result:'+result);
            resp.json(result);
        }
    })
});

module.exports = router;