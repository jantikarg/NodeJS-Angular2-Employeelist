const mongoose = require('mongoose');

const EmployeeSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    dob:{
        type: String,
        required: true
    },
    department:{
        type: String,
        required: true
    },
    gender:{
        type: String,
        required: true
    },
    age:{
        type: String,
        required: true
    }

});

const Employee = module.exports = mongoose.model('Employee', EmployeeSchema);